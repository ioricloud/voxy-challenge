terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}

resource "aws_iam_role" "iam_lambda" {
  name = "iam_lambda"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy" "iam_log" {
  name        = "iam_log"
  path        = "/"
  description = "IAM policy for logging stack from a lambda function"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*",
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "iam_kinesis_stream" {
  name        = "iam_kinesis_stream"
  path        = "/"
  description = "IAM policy for kinesis stream to Lambda"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "kinesis:ListShards",
                "kinesis:ListStreams",
                "kinesis:*"
            ],
            "Resource": "arn:aws:kinesis:*:*:*",
            "Effect": "Allow"
        },
        {
            "Action": [
                "stream:GetRecord",
                "stream:GetShardIterator",
                "stream:DescribeStream",
                "stream:*"
            ],
            "Resource": "arn:aws:stream:*:*:*",
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "logs_lambda" {
  role       = aws_iam_role.iam_lambda.name
  policy_arn = aws_iam_policy.iam_log.arn
}

resource "aws_iam_role_policy_attachment" "kinesis_lambda" {
  role       = aws_iam_role.iam_lambda.name
  policy_arn = aws_iam_policy.iam_kinesis_stream.arn
}

resource "aws_kinesis_stream" "kinesis" {
  name             = "terraform-kinesis"
  shard_count      = 2
  retention_period = 24

  shard_level_metrics = [
    "user_id",
    "user_activity",
    "user_score",
    "user_timespent",
  ]

  tags = {
    CreatedBy = "terraform-kinesis"
  }
}

resource "null_resource" "zip" {
  provisioner "local-exec" {
    command = "zip lambda_function.zip lambda_function.py"
  }
}

resource "aws_lambda_function" "lambda_function" {
  filename      = "lambda_function.zip"
  function_name = "terraform-kinesis-lambda"
  role          = aws_iam_role.iam_lambda.arn
  handler       = "lambda_function.lambda_handler"

  source_code_hash = filebase64sha256("lambda_function.zip")

  runtime = "python3.8"

  environment {
    variables = {
      DATA_STREAM_NAME = "user_kinesis"
    }
  }
}

resource "aws_lambda_function_event_invoke_config" "kinesis_invoke" {
  function_name = aws_lambda_function.lambda_function.function_name
  maximum_event_age_in_seconds = 60
  maximum_retry_attempts = 0
}

resource "aws_lambda_event_source_mapping" "kinesis_source" {
  event_source_arn = aws_kinesis_stream.kinesis.arn
  function_name = aws_lambda_function.lambda_function.arn
  starting_position = "LATEST"

  depends_on = [
    aws_iam_role_policy_attachment.kinesis_lambda
  ]
}