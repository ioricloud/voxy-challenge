import json

def lambda_handler(event, context):
    print(f"[lambda_function] Received Event: {json.dumps(event)}")
    return {
        'message': 'Completed'
    }