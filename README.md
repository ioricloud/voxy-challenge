# Challenge Accept
## Voxy - DevOps Job Position

This README is about devops challenge the Voxy Company. If I understand, this challenge that to create interlink in AWS Lambda and Event Stream(anyone), so, I used the suggestion, using AWS Kinesis, and today, I had to study about Kinesis and how I must using two services in interconnection. 

Actually, I use few time Lambda Function, and I knew how it behave. But, I didnt any about Kinesis. How part the challenge, I read many documentations and I tried to use Terraform to deploy this challenge automation fast way.

---
### Tips

This automation I used IaC (Infrastructure as Code). What do you need? Simple, just Terraform binary.

Windows Installation:

[Windows](https://www.terraform.io/downloads)

You must to install to Archteture reference, another installation is using Chocolatey.

[Chocolatey](https://learn.hashicorp.com/tutorials/terraform/install-cli)

Chocolatey is like APT package management for Windows Systems.
> choco install terraform

[Linux](https://learn.hashicorp.com/tutorials/terraform/install-cli)

Using this link to see many ways to install Terraform CLI in your Linux System.

[Mac](https://learn.hashicorp.com/tutorials/terraform/install-cli)

Using this link to see many ways to install Terraform CLI in your MacOS System.
*brew and binary*

---
### Deploy

How to deploy this challenge? With binary installed in your system, do you need to clone this repo, after this clone, to go in AWS Console, that to need have access and secret keys, that is CLI authorization, here I use Administration Access. After that this information, keep that, because, secret just see on time, keep that and use both in `variables.tf` file, or just execute deploy and in terminal that CLI ask this information.

First Step
> terraform init

This command that to start for download binaries that terraform needs to deploy.

Second Step
> terraform plan

This command that will plan in AWS Player, will see entire briefing that Terraform will create in AWS.

Third Step
> terraform apply

This command to execute deploy.

By the way, if you dont want more this infrastructure. You can to destroy it.

> terraform destroy

---
### Conclusion

Thank you and I stay for doubts.