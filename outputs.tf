output "kinesis_stream_name" {
    value = aws_kinesis_stream.kinesis.name
    description = "Kinesis Data Stream"
}